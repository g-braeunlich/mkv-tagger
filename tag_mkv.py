#!/usr/bin/env python3

from __future__ import annotations

import json
import os
import re
import subprocess
import sys
import tempfile
import unicodedata
import urllib.parse
import urllib.request
import warnings
import xml.sax
from dataclasses import dataclass, field, fields
from typing import TYPE_CHECKING, Any, TypeVar

if TYPE_CHECKING:
    from collections.abc import Callable, Iterable, Sequence


@dataclass
class Tags:
    title: str | None = None
    original_title: str | None = field(
        default=None, metadata={"serialized_name": "ORIGINALTITLE"}
    )
    date_released: int | None = None
    director: list[str] | None = None
    imdb_url: str | None = field(metadata={"serialized_name": "IMDBURL"}, default=None)
    tmdb_url: str | None = field(default=None, metadata={"serialized_name": "TMDBURL"})
    custom_tags: dict[str, str] = field(default_factory=dict)

    @classmethod
    def from_dict(cls, data: dict) -> Tags:
        conversions = {
            f.metadata.get("serialized_name", f.name.upper()): f.name
            for f in fields(cls)
            if f.name != "custom_tags"
        }
        kwargs = {
            conversions[key]: val for key, val in data.items() if key in conversions
        }
        if kwargs.get("director") == "n/a":
            del kwargs["director"]
        return cls(
            **kwargs,
            custom_tags={
                key: val for key, val in data.items() if key not in conversions
            },
        )

    def to_iter(self) -> Iterable[tuple[str, str | int | list[str]]]:
        items = fields(type(self))
        yield from (
            (
                f.metadata.get("serialized_name", f.name.upper()),
                getattr(self, f.name),
            )
            for f in items
            if getattr(self, f.name) is not None and f.name != "custom_tags"
        )
        yield from ((key, val) for key, val in self.custom_tags.items())

    def to_dict(self) -> dict[str, str | int | list[str]]:
        return dict(self.to_iter())


def subcmd_fetch(
    url_or_id: str,
    tmdb_url: str | None = None,
) -> None:
    if url_or_id.startswith("https://"):
        info = fetch(url_or_id, tmdb_url)
    elif url_or_id.endswith((".xml", ".json")):
        info = info_from_imdb_id(imdb_id_from_filename(url_or_id), tmdb_url)
    else:
        info = info_from_imdb_id(url_or_id, tmdb_url)
    if info is None:
        raise InternalError("Could not fetch metadata")
    if url_or_id.endswith(".xml"):
        with open(url_or_id, "w", encoding="utf-8") as f_xml:
            f_xml.write(xml_serialize(info))
    elif url_or_id.endswith(".json"):
        with open(url_or_id, "w", encoding="utf-8") as f_json:
            json.dump({"tags": info.to_dict()}, f_json, indent=2)
    else:
        print(xml_serialize(info))


def subcmd_tag_mkv(
    path: str,
    tags: str | None = None,
    imdb_id: str | None = None,
    tmdb_url: str | None = None,
    title: str | None = None,
    custom_tags: dict[str, str] | None = None,
) -> None:
    check_mkv_info(path)

    info: Tags | None
    if tags is not None:
        with open(tags, "rb") as stream:
            info = Tags.from_dict(json.load(stream)["tags"])
    else:
        info = info_from_imdb_id(imdb_id or imdb_id_from_filename(path), tmdb_url)
    if title is not None:
        info.title = title
    if custom_tags is not None:
        info.custom_tags = custom_tags
    tag_mkv(path, info)


def tag_mkv(path: str, info: Tags):
    temp_f = tempfile.NamedTemporaryFile(delete=False)
    with temp_f as f:
        f.write(xml_serialize(info).encode())
    subprocess.run(
        ("mkvpropedit", path, "--tags", "global:" + f.name),
        check=True,
        capture_output=True,
    )
    os.unlink(f.name)


def check_mkv_info(
    path: str, lang_by_track_number: dict[int, str] | None = None
) -> list[str]:
    if not path.endswith(".mkv"):
        raise InternalError("💥 Only mkv files are supported!")
    proc = subprocess.run(
        ("mkvmerge", "-F", "json", "-i", path),
        check=True,
        capture_output=True,
    )
    info = json.loads(proc.stdout)
    languages = []
    if lang_by_track_number is None:
        lang_by_track_number = {}
    for track in info["tracks"]:
        kind = track["type"]
        if kind not in {"audio", "subtitles"}:
            continue
        lang = track["properties"].get("language")
        if lang is None or lang == "und" or lang == "mis":
            number = track["properties"]["number"]
            track_id = track["id"]
            lang = lang_by_track_number.get(number)
            if lang is None:
                lang = input(
                    f"{kind} track number {number} (id: {track_id}) has lang {lang}. Please correct (explicitly type 'und' / 'mis' if it is a silent film / multi-language film):"
                )
            subprocess.run(
                (
                    "mkvpropedit",
                    path,
                    "--edit",
                    f"track:{number}",
                    "--set",
                    f"language={lang}",
                ),
                check=True,
            )
        languages.append(lang)
        if kind == "subtitles" and track["properties"].get("default_track", False):
            number = track["properties"]["number"]
            subprocess.run(
                (
                    "mkvpropedit",
                    path,
                    "--edit",
                    f"track:{number}",
                    "--set",
                    "flag-default=0",
                ),
                check=True,
            )
    return languages


def imdb_id_from_filename(path: str) -> str:
    query = "".join(
        c
        for c in os.path.basename(path).rsplit(".", 1)[0].replace(" ", "_").lower()
        if c.isidentifier() or c.isdigit()
    )
    return search_imdb(query)


def imdb_episode_id(imdb_series_id: str, season: int, episode: int) -> str:
    html = get(
        f"https://www.imdb.com/title/{imdb_series_id}/episodes/?season={season}"
    ).decode()
    pos = 0
    start_pattern = 'href="/title/'
    end_pattern = f'/?ref_=ttep_ep{episode}"'
    pos = html.find(end_pattern)
    start_pos = html.rfind(start_pattern, 0, pos)
    if pos == -1 or start_pos == -1:
        msg = f"💥 Could not determine imdb episode id for {imdb_series_id} season {season}, episode {episode}"
        raise InternalError(msg)
    return html[start_pos + len(start_pattern) : pos]


def info_from_imdb_id(imdb_id: str, tmdb_url: str | None = None) -> Tags:
    info = fetch(f"https://www.imdb.com/title/{imdb_id}/", tmdb_url)
    if info is None:
        msg = f"💥 Could not fetch imdb metadata for {imdb_id}"
        raise InternalError(msg)
    return info


def imdb_url_by_id(imdb_id: str) -> str:
    return f"https://www.imdb.com/title/{imdb_id}/"


def fetch(url: str, tmdb_url: str | None) -> Tags | None:
    tags = fetch_imdb(url)
    if tags is None:
        return None
    if tmdb_url is not None:
        tmdb_data: Sequence[tuple[str | None, str]] = [
            (fetch_tmdb_by_url(tmdb_url), tmdb_url)
        ]
    else:
        if tags.original_title is None or tags.date_released is None:
            tmdb_data = ()
        else:
            tmdb_data = fetch_tmdb(tags.original_title, tags.date_released)
        if not tmdb_data:
            tmdb_url = input("No TMDB data found. Please enter URL: ")
            tmdb_title = fetch_tmdb_by_url(tmdb_url)
            if tmdb_title is None:
                warnings.warn("⚠️ Could not determine TMDB title!", stacklevel=0)
                tags.tmdb_url = tmdb_url
                return tags
            tmdb_data = [(tmdb_title, tmdb_url)]
    tags.title, tags.tmdb_url = select_entry(tmdb_data)
    return tags


def select_entry(entries: Sequence[tuple[str | None, str]]) -> tuple[str | None, str]:
    try:
        [(title, url)] = entries
    except ValueError:
        print("Multiple results found:", file=sys.stderr)
        option = select(
            options=[{"title": title, "url": url} for title, url in entries],
            fmt="[{index}] {title} ({url})",
        )
        return option["title"], option["url"]
    return title, url


def xml_serialize(data: Tags) -> str:
    key_value_template = """    <Simple>
      <Name>{key}</Name>
      <String>{value}</String>
    </Simple>"""
    body = "\n".join(
        key_value_template.format(key=key, value=lst_to_str(val))
        for key, val in data.to_iter()
    )
    return f"""<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE Tags SYSTEM "matroskatags.dtd">
<Tags>
  <Tag>
    <Targets>
      <TargetTypeValue>50</TargetTypeValue>
    </Targets>
{body}
  </Tag>
</Tags>
"""


def lst_to_str(lst):
    if isinstance(lst, list):
        return "\n".join(lst)
    return lst


def search_imdb(query: str) -> str:
    query = (
        "".join(
            c
            for c in unicodedata.normalize("NFD", query)
            if unicodedata.category(c) != "Mn"
        )
        .encode("ascii", "ignore")
        .decode()
    )
    try:
        url = f"https://v2.sg.media-imdb.com/suggestion/{query[0]}/{query[:19]}.json"
        with urllib.request.urlopen(url) as f:
            suggestions = json.loads(f.read())
    except urllib.error.HTTPError as e:
        msg = f"Could not search imdb: queried url: {url}, response: {e}"
        raise InternalError(msg) from e
    movie_suggestions = [s for s in suggestions["d"] if s.get("id").startswith("tt")]
    for suggestion in movie_suggestions:
        suggestion.setdefault("s", "?")
        suggestion.setdefault("y", "?")
    option = select(movie_suggestions, fmt="[{index}] {l} ({y}) - {s}")
    return option["id"]


def fetch_imdb(url: str) -> Tags | None:
    try:
        html = get(url)
    except urllib.error.HTTPError as e:
        msg = f"Could not fetch imdb metadata. Url: {url}, response: {e}"
        raise InternalError(msg) from e
    start_marker = b'<script type="application/ld+json">'
    end_marker = b"</script>"
    start = html.find(start_marker)
    if start == -1:
        return None
    end = html.find(end_marker, start)
    json_raw = html[start + len(start_marker) : end]
    data = json.loads(json_raw)
    date_released_raw = data.get("datePublished")
    if date_released_raw is None:
        date_released = int(
            input("Could not determine IMDB release date. Please enter manually: ")
        )
    else:
        date_released = int(date_released_raw.split("-", 1)[0])
    director = [d["name"] for d in data["director"]] if "director" in data else None
    return Tags(
        date_released=date_released,
        director=director,
        title=data["name"],
        original_title=data["name"],
        imdb_url=url,
    )


def fetch_tmdb(name: str, year: int | None) -> list[tuple[str, str]]:
    result_regex = re.compile(
        rb"""<a data-id="[0-9a-f]+" data-media-type="(?:movie|tv)" data-media-adult="(?:true|false)" class="result" href="([^"]+)"><h2>([^<]+)</h2></a>"""
    )
    query = urllib.parse.quote(f"{name} y:{year}" if year is not None else name)
    html = get(f"https://www.themoviedb.org/search?query={query}")
    return [
        (title.decode(), "https://www.themoviedb.org" + url.decode())
        for url, title in result_regex.findall(html)
    ]


def fetch_tmdb_by_url(url: str) -> str | None:
    result_regexes = (
        re.compile(rb"""<title>([^<]*) - [A-Za-z]* @ omdb</title>"""),
        re.compile(rb"""<meta property="og:title" content="([^"]*)">"""),
        re.compile(
            rb"""<title>([^<]*) \([0-9]{4}\) &#8212; The Movie Database (TMDB)</title>"""
        ),
    )
    html = get(url)
    for result_regex in result_regexes:
        match = result_regex.search(html)
        if match is None:
            continue
        (title,) = match.groups()
        return title.decode()
    return None


def fetch_tmdb_series_titles(url: str, lang: Lang | None) -> dict[tuple[int, int], str]:
    url_path = urllib.parse.urlparse(url).path
    if lang is not None:
        url += f"?language={lang.iso_639_1_code}"
    html = get(url).decode()
    pattern = f'href="{url_path}'
    pos = 0
    titles = {}
    while True:
        tag_slc = xml_tag_interior_by_pattern(html, pattern, pos)
        if tag_slc is None:
            break
        tag_interior = html[tag_slc]
        season = parse_attr(tag_interior, "season")
        episode = parse_attr(tag_interior, "episode")
        if season is None or episode is None:
            pos = tag_slc.stop
            continue
        after_tag = tag_slc.stop + 1
        end_tag = html.find("<", after_tag)
        title = html[after_tag:end_tag].strip()
        pos = end_tag
        if not title:
            continue
        titles[(int(season), int(episode))] = title

    return titles


def xml_tag_interior_by_pattern(html: str, pattern: str, start: int) -> slice | None:
    pos = html.find(pattern, start)
    if pos == -1:
        return None
    tag_end = html.find(">", pos)
    tag_start = html.rfind("<", 0, pos)
    if tag_end == -1 or tag_start == -1:
        return None
    return slice(tag_start + 1, tag_end)


def parse_attr(s: str, attribute_name: str) -> str | None:
    pos = s.find(f'{attribute_name}="')
    if pos == -1:
        return None
    pos += len(attribute_name) + 2
    end_pos = s.find('"', pos)
    return s[pos:end_pos]


def get(url: str, debuglevel=0):
    request = urllib.request.Request(
        url,
        headers={"User-Agent": "Mozilla"},
    )
    opener = urllib.request.build_opener(
        urllib.request.HTTPSHandler(debuglevel=debuglevel)
    )
    with opener.open(request) as f:
        return f.read()


def select(options: list, fmt: str):
    print(
        "\n".join(
            fmt.format(index=index, **item) for index, item in enumerate(options)
        ),
        file=sys.stderr,
    )
    index = int(input("Index: "))
    return options[index]


def extract_mkv_metadata(path: str) -> dict[str, Any]:
    return {**load_media_info(path), "tags": extract_mkv_tags(path).to_dict()}


def load_media_info(path: str) -> dict[str, Any]:
    proc = subprocess.run(
        ("mkvmerge", "-F", "json", "-i", path),
        check=True,
        capture_output=True,
    )
    info = json.loads(proc.stdout)

    def extract_lang(kind: str) -> list[str | None]:
        return [
            track["properties"].get("language")
            for track in info["tracks"]
            if track["type"] == kind
        ]

    return {
        "resolution": next(
            track["properties"]["pixel_dimensions"]
            for track in info["tracks"]
            if track["type"] == "video"
        ),
        "audio_tracks": extract_lang("audio"),
        "subtitle_tracks": extract_lang("subtitles"),
    }


def extract_mkv_tags(path: str) -> Tags:
    proc = subprocess.run(
        ("mkvextract", path, "tags", "-"),
        check=True,
        capture_output=True,
    )
    return Tags.from_dict(parse_xml_tags(proc.stdout))


@dataclass(frozen=True)
class Lang:
    name: str
    iso_639_3_code: str
    iso_639_1_code: str


def load_mkv_languages() -> Iterable[Lang]:
    proc = subprocess.run(
        ("mkvmerge", "--list-languages"),
        check=True,
        capture_output=True,
    )
    lines = iter(proc.stdout.decode().splitlines())
    next(lines)  # header
    next(lines)  # separator
    for line in lines:
        name, iso_3, _, iso_1 = line.strip().split("|")
        yield Lang(name.strip(), iso_3.strip(), iso_1.strip())


def tag_series_episodes(
    paths: list[str],
    config: Config,
    imdb_id: str | None = None,
    tmdb_url: str | None = None,
    custom_tags: dict[str, str] | None = None,
    lang_by_track_number: dict[int, str] | None = None,
    *,
    rename: bool = True,
) -> None:
    directories = {p for p in paths if os.path.isdir(p)}
    paths = [p for p in paths if p not in directories] + [
        p for d in directories for p in os.listdir(d) if p.endswith(".mkv")
    ]
    episodes = match_episodes(paths, config.series_regexes, lang_by_track_number or {})
    for series, series_episodes in group_by(episodes, lambda row: row.series).items():
        series_id = imdb_id or imdb_id_from_filename(series)
        if tmdb_url is None:
            series_tags = fetch_imdb(imdb_url_by_id(series_id))
            candidates = (
                fetch_tmdb(series_tags.title, series_tags.date_released)
                if series_tags is not None and series_tags.title is not None
                else []
            )
            _, tmdb_url = select_entry(candidates)
        if tmdb_url.find("/season/") == -1:
            tmdb_series_url = tmdb_url
        else:
            tmdb_series_url = tmdb_url[: tmdb_url.find("/season/")]
        for (season, lang), season_episodes in group_by(
            series_episodes, lambda row: (row.season, row.language)
        ).items():
            tmdb_season_url = f"{tmdb_series_url}/season/{season}"
            titles = fetch_tmdb_series_titles(tmdb_season_url, lang)
            for episode in season_episodes:
                tmdb_episode_url = f"{tmdb_season_url}/episode/{episode.episode}"
                title = titles.get((season, episode.episode))
                if title is None:
                    print(
                        f"💥 Could not fetch TMDB title for {tmdb_episode_url}",
                        file=sys.stderr,
                    )
                    continue
                try:
                    info = info_from_imdb_id(
                        imdb_episode_id(series_id, season, episode.episode),
                        tmdb_episode_url,
                    )
                except InternalError as e:
                    print(format(e), file=sys.stderr)
                    continue
                info.title = title
                if custom_tags is not None:
                    info.custom_tags = custom_tags
                tag_mkv(episode.path, info)
                if rename:
                    dest = config.series_rename_pattern.format(
                        title=info.title,
                        season=season,
                        episode=episode.episode,
                        series=series,
                    )
                    move(episode.path, dest)
                    print("ℹ️", episode.path, "->", dest)


T = TypeVar("T")
U = TypeVar("U")


def group_by(items: Iterable[T], key: Callable[[T], U]) -> dict[U, list[T]]:
    grouped: dict[U, list[T]] = {}
    for item in items:
        k = key(item)
        grouped.setdefault(k, []).append(item)
    return grouped


@dataclass
class EpisodeMatch:
    path: str
    series: str
    season: int
    episode: int
    language: Lang | None


def match_episodes(
    paths: Iterable[str],
    series_regexes: Sequence[str],
    lang_by_track_number: dict[int, str],
) -> Iterable[EpisodeMatch]:
    language_mapping = {lang.iso_639_3_code: lang for lang in load_mkv_languages()}
    for path in paths:
        languages = check_mkv_info(path, lang_by_track_number)
        dominant_lang = language_mapping.get(languages[0])
        series, season, episode = match_episode(path, series_regexes)
        yield EpisodeMatch(path, series, season, episode, dominant_lang)


def match_episode(path: str, series_regexes: Sequence[str]) -> tuple[str, int, int]:
    for series_regex in series_regexes:
        m = re.compile(series_regex).match(path)
        if m is not None:
            series = m.groupdict()["series"]
            season = int(m.groupdict()["season"])
            episode = int(m.groupdict()["episode"])
            return series, season, episode
    msg = f"💥 Could not match {path} to episode pattern"
    raise InternalError(msg) from None


def move(src: str, dest: str) -> None:
    if dest == src:
        return
    folder, filename = os.path.split(dest)
    if folder:
        os.makedirs(folder, exist_ok=True)
    os.replace(src, dest)


class AttributeLessXmlHandler(xml.sax.ContentHandler):
    def __init__(self, out: dict):
        super().__init__()
        self.path: list[tuple[str, Any]] = [("", out)]

    def startElement(self, name, _attrs):  # noqa: N802
        if self.path[-1][1] is None:
            self.path[-1] = (self.path[-1][0], {})
        self.path.append((name, None))

    def endElement(self, name):  # noqa: N802
        assert name == self.path[-1][0], f"{name} != {self.path[-1][0]}"
        name, val = self.path.pop()
        parent = self.path[-1][1]
        existing_node = parent.get(name)
        if existing_node is not None:
            if not isinstance(existing_node, list):
                parent[name] = existing_node = [existing_node]
            existing_node.append(val)
        else:
            parent[name] = val

    def characters(self, content):
        if not content.strip():
            return
        self.path[-1] = (self.path[-1][0], content)


def parse_xml_tags(xml_source: bytes) -> dict[str, str]:
    if not xml_source.strip():
        return {}
    metadata: dict[str, Any] = {}
    xml.sax.parseString(xml_source, AttributeLessXmlHandler(metadata))
    try:
        tag_entries = next(t for t in metadata["Tags"]["Tag"] if t["Targets"] is None)[
            "Simple"
        ]
        tags = {entry["Name"]: entry["String"] for entry in tag_entries}
    except (StopIteration, KeyError, TypeError):
        return {}
    return tags


def edit(path: str) -> None:
    metadata = extract_mkv_tags(path)
    temp_f = tempfile.NamedTemporaryFile(delete=False, mode="w")
    with temp_f as stream:
        json.dump(metadata.to_dict(), stream, indent=2)
    subprocess.run((os.environ.get("EDITOR", "/usr/bin/nano"), temp_f.name), check=True)
    with open(temp_f.name, encoding="utf-8") as stream:
        metadata = Tags.from_dict(json.load(stream))
    os.unlink(temp_f.name)
    tag_mkv(path, metadata)


def subcmd_extract(path: str, out: str | None, filter_paths: Sequence[str]):
    if os.path.isdir(path):
        if out is None:
            raise InternalError("out argument must be specified if path is a directory")
        extract_folder_metadata(
            path,
            out=out,
            filter_paths=filter_paths,
        )
    else:
        metadata = extract_mkv_metadata(path)
        if out is None:
            json.dump(metadata, sys.stdout, indent=2)
        else:
            with open(out, "w", encoding="utf-8") as stream:
                json.dump(metadata, stream, indent=2)


def extract_folder_metadata(
    path: str,
    out: str,
    filter_paths: Sequence[str] = (),
):
    ext = ".mkv"
    for dirpath, _dirnames, filenames in os.walk(path):
        if not any(f.endswith(ext) for f in filenames) or any(
            f in dirpath for f in filter_paths
        ):
            continue
        dest_path = os.path.join(out, os.path.relpath(dirpath, path))
        os.makedirs(dest_path, exist_ok=True)
        for f_name in filenames:
            if not f_name.endswith(ext) or any(f in f_name for f in filter_paths):
                continue
            try:
                metadata = extract_mkv_metadata(os.path.join(dirpath, f_name))
            except subprocess.CalledProcessError as e:
                sys.stderr.write(
                    f"⚠️ {os.path.join(dirpath, f_name)}: Could not read metadata: {e}\n"
                )
                continue
            with open(
                os.path.join(dest_path, f_name + ".json"), "w", encoding="utf-8"
            ) as stream:
                json.dump(metadata, stream, indent=2)


class InternalError(Exception):
    """Exception which will be caught by the CLI."""


@dataclass
class Config:
    """Representation of the config file."""

    series_regexes: Sequence[str] = ()
    series_rename_pattern: str = "{series}/{season}-{episode:02} {title}.mkv"


def load_config() -> Config:
    config_path = os.path.expanduser(
        os.path.join("~", ".config", "tag_mkv", "config.json")
    )
    try:
        with open(config_path, "rb") as stream:
            cfg = json.load(stream)
    except FileNotFoundError:
        cfg = {}
    return Config(**cfg)


if __name__ == "__main__":
    import argparse

    def _dict_insert_action(
        key_type: type = str, val_type: type = str
    ) -> type[argparse.Action]:
        class _DictInsertAction(argparse.Action):
            def __call__(self, _parser, namespace, values, _option_string=None) -> None:
                key, val = values.split("=", 1)
                if getattr(namespace, self.dest) is None:
                    setattr(namespace, self.dest, {})
                getattr(namespace, self.dest)[key_type(key)] = val_type(val)

        return _DictInsertAction

    _path_argument = argparse.ArgumentParser(add_help=False)
    _path_argument.add_argument("path", help="mkv file")

    _subcmd_parser = argparse.ArgumentParser(exit_on_error=False)
    _subparsers = _subcmd_parser.add_subparsers(
        dest="subcmd",
        title="List of sub-commands",
        description="For an overview of action specific parameters, "
        "use %(prog)s <SUB-COMMAND> --help",
        help="Sub-command help",
        metavar="<SUB-COMMAND>",
    )
    _subparser = _subparsers.add_parser(
        "edit",
        help="Open an editor to edit metadata, save back to file",
        parents=[_path_argument],
    )
    _subparser.set_defaults(subcmd=edit)
    _subparser = _subparsers.add_parser(
        "check",
        help="Check audio / subtitle metadata and exit",
        parents=[_path_argument],
    )
    _subparser.set_defaults(subcmd=check_mkv_info)
    _subparser = _subparsers.add_parser(
        "extract",
        help="Extract metadata of a mkv file or a folder containing mkv files",
        parents=[_path_argument],
    )
    _subparser.set_defaults(subcmd=subcmd_extract)
    _subparser.add_argument("-o", dest="out", help="Output directory / file")
    _subparser.add_argument(
        "--filter",
        dest="filter_paths",
        action="append",
        default=[],
        help="Filters to be applied to paths",
    )
    _subparser = _subparsers.add_parser(
        "fetch",
        help="fetch imdb / tmdb metadata and print as xml to stdout",
    )
    _subparser.set_defaults(subcmd=subcmd_fetch)
    _subparser.add_argument("url_or_id", help="IMDB url or id")
    _subparser.add_argument("--tmdb_url", help="Specify tmdb url")

    _custom_tag_arguments = argparse.ArgumentParser(add_help=False)
    _custom_tag_arguments.add_argument(
        "--tag",
        dest="custom_tags",
        help="Add a custom tag",
        action=_dict_insert_action(),
    )

    _subparser = _subparsers.add_parser(
        "tag-series",
        help="tag series episodes",
        parents=[_custom_tag_arguments],
    )
    _config = load_config()
    _subparser.set_defaults(subcmd=tag_series_episodes, config=_config)
    _subparser.add_argument("paths", nargs="+", help="mkv files and or directories")
    _subparser.add_argument(
        "--imdb-id", help="Specify the IMDB id of the series", default=None
    )
    _subparser.add_argument(
        "--tmdb-url", help="Specify the TMBD URL of the series", default=None
    )
    _subparser.add_argument(
        "--rename",
        help="Rename the file according to config",
        default=False,
        action=argparse.BooleanOptionalAction,
    )
    _subparser.add_argument(
        "--set-language",
        action=_dict_insert_action(int, str),
        dest="lang_by_track_number",
        help="Set language of track with the specified number to lang if it is currently 'und'",
    )
    _tag_arguments = argparse.ArgumentParser(
        add_help=False, parents=[_path_argument, _custom_tag_arguments]
    )
    _tag_arguments.add_argument("--tags", help="Read tags from json file", default=None)
    _tag_arguments.add_argument("--imdb-id", help="Specify the IMDB id", default=None)
    _tag_arguments.add_argument("--tmdb-url", help="Specify the TMBD URL", default=None)
    _tag_arguments.add_argument("--title", help="Manually specify title", default=None)
    _subparsers.add_parser(
        "tag",
        help="tag a mkv file or write out json / xml files",
        parents=[_tag_arguments],
    ).set_defaults(subcmd=subcmd_tag_mkv)

    try:
        try:
            _args = vars(_subcmd_parser.parse_args())
            _subcmd = _args.pop("subcmd")
            if _subcmd is None:
                _subcmd_parser.print_help()
                sys.exit(0)
            _subcmd(**_args)
        except argparse.ArgumentError as err:
            if not str(err).startswith("argument <SUB-COMMAND>: invalid choice:"):
                _subcmd_parser.error(str(err))
            # Add help:
            _tag_arguments = argparse.ArgumentParser(parents=[_tag_arguments])
            _args = vars(_tag_arguments.parse_args())
            subcmd_tag_mkv(**_args)
    except InternalError as err:
        print(err.args[0], file=sys.stderr)
        sys.exit(1)
    sys.exit(0)
